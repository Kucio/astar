#include "utils.h"

#include <fstream>
#include <sstream>

std::tuple<Grid, Point, Point> readFromFile(std::string_view inputFile)
{
  std::ifstream inFile;

  inFile.open(inputFile.data());

  std::tuple<Grid, Point, Point> output;
  std::string line;

  Grid outputGrid;
  Point start;
  Point end;

  // load from file to loadedNumbers
  unsigned int rowCounter = 0;
  unsigned int colCounter = 0;
  while (getline(inFile, line))
  {
    std::stringstream linestream(line);
    std::string value;
    std::vector<int> loadedNumbers;

    while (getline(linestream, value, ','))
    {
      auto numericValue = std::stoi(value);

      if(numericValue == POINTS::START)
      {
        start.first = rowCounter;
        start.second = colCounter;
      }
      else if(numericValue == POINTS::END)
      {
        end.first = rowCounter;
        end.second = colCounter;
      }

      loadedNumbers.push_back(numericValue);
      colCounter++;
    }
    //copy current row to output grid
    for(unsigned int i = 0; i < loadedNumbers.size(); i++)
    {
      outputGrid[rowCounter][i] = loadedNumbers[i];
    }
    rowCounter++;
    colCounter = 0;
  }

  inFile.close();
  output = std::make_tuple(outputGrid, start, end);
  return output;
}

AsciiGrid convert_to_ascii(const Grid &grid)
{
  AsciiGrid result;

  // convert int grid to ascii
  for (int i = 0; i < result.size(); i++)
  {
    std::transform(grid[i].begin(), grid[i].end(), result[i].begin(), [](int elem)
                   {
    if(elem == 0)
      return '.';
    else if(elem == 1)
      return '/';
    else if(elem == 2)
      return 'S';
    else if(elem == 3)
      return 'E';
    throw std::runtime_error("UNEXPECTED");});
  }

  return result;
}

AsciiGrid print_ascii_map(const Grid& grid, const Path& path)
{
  AsciiGrid ascii_rep = convert_to_ascii(grid);
  unsigned int score = 0;
  //mark path on ascii map with skip of start and end points
  for (auto coord = path.begin(); coord != path.end(); ++coord)
  {
    if(coord == path.begin() || std::next(coord) == path.end())
      continue;
    ascii_rep[coord->first][coord->second] = '+';
    score++;
  }

  //print map
  for(int i = 0; i < ascii_rep.size(); i++)
  {
    for(int j = 0; j < ascii_rep[0].size(); j++)
    {
      std::cout << ascii_rep[i][j] << " ";
    }
    std::cout << std::endl;
  }
  //+1 to include score to the same end
  std::cout << "Score: " << score + 1 << std::endl;
  return ascii_rep;
}

void output_to_file(const AsciiGrid& grid)
{
  std::ofstream resultFile;
  resultFile.open("output.txt"); // opens the file

  if (!resultFile)
  { // file couldn't be opened
    std::cout << "Error: file could not be opened" << std::endl;
    exit(1);
  }

  for(const auto& row : grid)
  {
    for(const auto& val: row)
    {
      resultFile << val << " ";
    }
    resultFile << "\n";
  }
  resultFile.close();
}