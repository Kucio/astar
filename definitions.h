#pragma once

#include <algorithm>
#include <iostream>
#include <array>
#include <vector>
#include <math.h>
#include <queue>


typedef std::pair<int, int> Point;

typedef std::vector<Point> Path;

typedef std::array<std::array<int, 20>, 20> Grid;

typedef std::array<std::array<char, 20>, 20> AsciiGrid;

//Cell is a structure representing tuple of heuristics result
//for specific coordinates

struct HeurVal
{
  double f;
  double g;
  double h;
};

typedef std::tuple<HeurVal, int, int> Cell;

// Inner vector for priority queue to hold only unique values
template <typename T>
class UniqueVector : public std::vector<T>
{
public:
  using std::vector<T>::vector;

  void push_back(const T &value)
  {
    auto result = std::find_if(this->begin(), this->end(), [&](const T &foundValue)
                               { return (std::get<1>(foundValue) == std::get<1>(value)) && (std::get<2>(foundValue) == std::get<2>(value)); });
    if (result == this->end())
    {
      std::vector<T>::push_back(value);
    }
  }
};

enum POINTS
{
  LEGAL = 0,
  ILLEGAL = 1,
  START = 2,
  END = 3
};

// Global priority queue containing only unique points found while aStar algorithm search
struct
{
  bool operator()(const Cell &lhs, const Cell &rhs) const
  {
    return std::get<0>(lhs).f > std::get<0>(rhs).f;
  }
} custom_comparator;

static std::priority_queue<Cell, UniqueVector<Cell>, decltype(custom_comparator)> ptQueue;
