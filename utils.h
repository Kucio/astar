#pragma once
#include "definitions.h"
#include <tuple>
#include <fstream>

std::tuple<Grid, Point, Point> readFromFile(std::string_view inputFile);
AsciiGrid convert_to_ascii(const Grid &grid);
AsciiGrid print_ascii_map(const Grid& grid, const Path& path);
void output_to_file(const AsciiGrid& grid);