#include "aStar.h"
#include "utils.h"

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    std::cout << "Please provide input file" << std::endl;
    return 0;
  }

  auto gridSetup = readFromFile(argv[1]);
  Grid grid = std::get<0>(gridSetup);
  auto startNode = std::get<1>(gridSetup);
  auto endNode = std::get<2>(gridSetup);

  auto foundPath = AStar::findPath(grid, { {0.0, 0.0, 0.0}, startNode.first, startNode.second}, endNode);

  Path finalPath;
  AStar::traceBackPath(startNode, *foundPath.rbegin() ,foundPath, finalPath, grid);

  //print map with path
  std::cout << "Result: " << std::endl;
  std::cout << "S - Start point" << std::endl;
  std::cout << "E - End point" << std::endl;
  std::cout << ". - Not visited nodes" << std::endl;
  std::cout << "/ - Wall" << std::endl;
  std::cout << "+ - Path found by algorithm" << std::endl;
  std::cout << std::endl;
  auto ascii_rep = print_ascii_map(grid, finalPath);

  output_to_file(ascii_rep);
}
