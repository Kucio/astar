1. Compile just with g++ aStar.cpp utils.cpp main.cpp (C++17 needed)
2. Run ./a.out in.txt where in.txt is a 20x20 grid represended as comma separated sequence of numbers where each value represents:
         0 is a legal node
         1 is a illegal node
         2 is a start node,
         3 is end node
3. Enjoy :)