#pragma once

#include "definitions.h"

class AStar
{
public:
    static std::vector<Cell> findPath(const Grid &grid, const Cell &start, Point &end);
    static void traceBackPath(Point &start, Cell &end, std::vector<Cell> &foundPath,
                              Path &finalPath, Grid &grid);
    static double euclidianDistance(const Point &start, const Point &end);
    static bool inBound(const Point &point, const Grid &grid);
private:
    static bool alreadyVisited(const Point &point, const Path &path);
    static Cell getNextStep(std::vector<Cell> &points,
                             const Grid &grid, Point &end, Path &visited);
    static std::vector<Cell> generateNeighbouringCells(const Cell& point);
    static std::vector<Cell> getNeighbouringCells(const Cell &currentPoint, std::vector<Cell> &foundPath);
};