#include "aStar.h"

#include <algorithm>
#include <cfloat>

double AStar::euclidianDistance(const Point &start, const Point &end)
{
  auto dist = sqrt(
      pow((start.first - end.first), 2.0) +
      pow((start.second - end.second), 2.0));

  return dist;
}

bool AStar::alreadyVisited(const Point &point, const Path &path)
{
  for (auto &inPath : path)
  {
    if ((inPath.first == point.first &&
         inPath.second == point.second))
    {
      return true;
    }
  }

  return false;
}

bool AStar::inBound(const Point &point, const Grid &grid)
{
  return ((point.first <= grid.size() - 1) && (point.second <= grid[0].size() - 1));
}

Cell AStar::getNextStep(std::vector<Cell> &points, const Grid &grid, Point &end, Path &visited)
{
  Cell foundPoint;
  for (auto &point : points)
  {
    HeurVal val;
    val.g = std::get<0>(point).g + 1.41;
    
    const auto& x_val = std::get<1>(point);
    const auto& y_val = std::get<2>(point);
    const auto& coords = std::pair{x_val, y_val};

    val.h = euclidianDistance(coords, end);
    val.f = val.h + val.g;

    if (grid[x_val][y_val] != POINTS::ILLEGAL &&
        !alreadyVisited(coords, visited) &&
        inBound(coords, grid))
    {
      auto cellCandidate = std::make_tuple(val, coords.first, coords.second);
      ptQueue.push(cellCandidate);
    }
  }

  if (!ptQueue.empty())
  {
    std::get<0>(foundPoint) = std::get<0>(ptQueue.top());
    std::get<1>(foundPoint) = std::get<1>(ptQueue.top());
    std::get<2>(foundPoint) = std::get<2>(ptQueue.top());
    ptQueue.pop();
  }
  else
  {
    std::cout << "Error: Could not find any path" << std::endl;
    exit(1);
  }
  return foundPoint;
}

std::vector<Cell> AStar::generateNeighbouringCells(const Cell &currentPoint)
{
  // 0 - top
  // 1 - bottom
  // 2 - left
  // 3 - right
  // 4 - top right
  // 5 - top left
  // 6 - bottom left
  // 7 - bottom right

  const auto& currX = std::get<1>(currentPoint);
  const auto& currY = std::get<2>(currentPoint);
  auto newHeur = std::get<0>(currentPoint);

  Cell top = { newHeur , currX, currY - 1 };
  Cell bottom = { newHeur , currX, currY+ 1};
  Cell left = { newHeur , currX - 1, currY};
  Cell right = { newHeur , currX + 1, currY};

  Cell topRight = { newHeur , currX + 1, currY- 1};
  Cell topLeft = { newHeur , currX - 1, currY- 1};
  Cell bottomLeft = { newHeur , currX - 1, currY+ 1};
  Cell bottomRight = { newHeur , currX + 1, currY+ 1};

  return {top, bottom, right, left, topRight, topLeft, bottomLeft, bottomRight};
}

std::vector<Cell> AStar::getNeighbouringCells(const Cell &currentPoint, std::vector<Cell> &foundPath)
{
  const auto& currX = std::get<1>(currentPoint);
  const auto& currY = std::get<2>(currentPoint);
  std::vector<Cell> result;

  auto addToPath = [&foundPath, &result](int x, int y)
  {
    auto found = std::find_if(foundPath.begin(), foundPath.end(), [x,y](const auto& elemInPath)
    {
      return x == std::get<1>(elemInPath) &&
             y == std::get<2>(elemInPath);
    });
    if(found != foundPath.end())
    {
      result.push_back(*found);
    }
  };

  // 0 - top
  addToPath(currX, currY - 1 );
  // 1 - bottom
  addToPath(currX, currY + 1);
  // 2 - left
  addToPath(currX - 1, currY);
  // 3 - right
  addToPath(currX + 1, currY);
  // 4 - top right
  addToPath(currX + 1, currY - 1);
  // 5 - top left
  addToPath(currX - 1, currY - 1);
  // 6 - bottom left
  addToPath(currX - 1, currY + 1);
  // 7 - bottom right
  addToPath(currX + 1, currY + 1);

  return result;
}

std::vector<Cell> AStar::findPath(const Grid &grid, const Cell &start, Point &end)
{
  std::vector<Cell> path;
  Path visited;
  path.push_back(start);

  bool foundFinalPath = false;
  Cell currentPoint = start;
  std::vector<Cell> points;
  while (!foundFinalPath)
  {
    // check next step
    points = generateNeighbouringCells(currentPoint);
    Cell nextStep = getNextStep(points, grid, end, visited);

    Point nextPoint = {std::get<1>(nextStep), std::get<2>(nextStep)};

    path.push_back(nextStep);
    visited.push_back(nextPoint);
    currentPoint = std::move(nextStep);

    if (grid[nextPoint.first][nextPoint.second] == POINTS::END)
    {
      foundFinalPath = true;
    }
  }

  return path;
}

void AStar::traceBackPath(Point &start, Cell &currentCell, std::vector<Cell> &foundPath, Path &finalPath, Grid &grid)
{
  auto & bestCandidate = currentCell;
  auto score = DBL_MAX;
  finalPath.push_back({std::get<1>(bestCandidate), std::get<2>(bestCandidate)});
  std::vector<Cell> points = getNeighbouringCells(bestCandidate, foundPath);

  for (auto &point : points)
  {
    const auto &coords = std::pair{std::get<1>(point), std::get<2>(point)};
    if (!inBound(coords, grid) ||
        (grid[coords.first][coords.second] == POINTS::ILLEGAL) ||
        (std::find_if(foundPath.begin(), foundPath.end(), [&point](const auto &elem)
                      { return std::get<1>(elem) == std::get<1>(point) && std::get<2>(elem) == std::get<2>(point); }) == foundPath.end()) ||
        (std::find(finalPath.begin(), finalPath.end(), coords) != finalPath.end()))
    {
      continue;
    }

    if (coords == start)
    {
      // found target node
      finalPath.push_back(coords);
      return;
    }

    auto pathValue = std::get<0>(point).f;
    if (pathValue < score)
    {
      score = pathValue;
      bestCandidate = point;
    }
  }
  // recursive call with next best point found
  traceBackPath(start, bestCandidate, foundPath, finalPath, grid);
}
